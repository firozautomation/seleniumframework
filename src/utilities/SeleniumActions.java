package utilities;

public enum SeleniumActions {
	
	Click, 
	Type,
	AltType,
	TypeRandomValue,
	SelectByValue,
	SelectByText,
	SelectByIndex,
	Hightlight, 
	RobotKey,
	DragAndDrop,
	DragAndDropOnObject,
	ClickAlertIfExist, 
	SwitchToWindows,
	SwitchToFrame,
	Wait, 
	Navigate,
	Clear,
	Scroll;
}
